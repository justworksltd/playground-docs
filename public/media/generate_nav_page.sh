#!/bin/sh

# Step 0 - Remove previous if existing
rm -f index.html
touch index.html
# Step 1 - Create a basic HTML head and start the body
echo "
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Neodroid Playground - Media files</title>
  <meta name="description" content="Media files of Neodroid Playground">
  <meta name="author" content="Halvor Bakken Smedås">
  <link rel="stylesheet" href="css/styles.css?v=1.0">
</head>
<body>
<ul>
" >> index.html;
# Step 2 - Place all files into links
OIFS="$IFS";
IFS=$'\n';
find . -wholename './*' -exec sh -c '
newdir=true;
depth=0;
  for f do
        if [ -d "$f" ]; then
            if [ "$newdir" = true ]; then
                let "depth++"
                echo "<li>$(echo $f | sed "s/.*\///")<ul>" >> index.html
            else
                newdepth=$(echo $f | grep -o / | wc -l);
                for i in $(seq $newdepth $depth);
                do
                    echo "</ul>" >> index.html
                done
                depth=$newdepth
                echo "<li>$(echo $f | sed "s/.*\///")<ul>" >> index.html
                newdir=true
            fi
        elif [[ $(echo $f | grep -o / | wc -l) -ne 1 ]]; then
            newdir=false
            echo "<li><a href=\"$f\">$(echo $f | sed "s/.*\///")</a></li>" >> index.html
        fi
  done
' sh {} +
# Step 3 - Close of the body and html document
echo "
</ul>
</body>
</html>
" >> index.html;
